#!/bin/bash

env=$1

# Modified configs
modifiedConfigsList=$( git diff --diff-filter AM --name-only HEAD^ HEAD )
modifiedConfigsFolder="configs/modified-configs/env/"$env
rm -rf $modifiedConfigsFolder
mkdir -p $modifiedConfigsFolder

echo "[ " > $modifiedConfigsFolder/caches.json
echo "[ " > $modifiedConfigsFolder/kvms.json
echo "[ " > $modifiedConfigsFolder/targetServers.json

for var in ${modifiedConfigsList[@]}
	do 
	case "$var" in
		"caches/"*)
		echo "Preparing to create/update cache '$var'"
		echo "$(cat $var)," >> $modifiedConfigsFolder/caches.json ;;
		
		"keyvaluemaps/"*)
		echo "Preparing to create/update kvm '$var'"
		echo "$(cat $var)," >> $modifiedConfigsFolder/kvms.json ;;

		"targetservers/"*)
		echo "Preparing to create/update targetserver '$var'"
		echo "$(cat $var)," >> $modifiedConfigsFolder/targetServers.json ;;

		*) ;;
	esac
done

sed -i "$ s/.$//" $modifiedConfigsFolder/caches.json
echo "]" >> $modifiedConfigsFolder/caches.json

sed -i "$ s/.$//" $modifiedConfigsFolder/kvms.json
echo "]" >> $modifiedConfigsFolder/kvms.json

sed -i "$ s/.$//" $modifiedConfigsFolder/targetServers.json
echo "]" >> $modifiedConfigsFolder/targetServers.json


# Deleted configs
deletedConfigsList=$( git diff --diff-filter D --name-only HEAD^ HEAD )
deletedConfigsFolder="configs/deleted-configs/env/"$env
rm -rf $deletedConfigsFolder
mkdir -p $deletedConfigsFolder

echo "[ " > $deletedConfigsFolder/caches.json
echo "[ " > $deletedConfigsFolder/kvms.json
echo "[ " > $deletedConfigsFolder/targetServers.json

for var in ${deletedConfigsList[@]}
	do 
	case "$var" in
		"caches/"*)
		config=$(echo $var | sed -e "s/caches\///g")
		echo "Preparing to delete cache '$config'"
		echo "{\"name\":\"$config\"}," >> $deletedConfigsFolder/caches.json ;;
		
		"keyvaluemaps/"*)
		config=$(echo $var | sed -e "s/keyvaluemaps\///g")
		echo "Preparing to delete kvm '$config'"
		echo "{\"name\":\"$config\"}," >> $deletedConfigsFolder/kvms.json ;;

		"targetservers/"*)
		config=$(echo $var | sed -e "s/targetservers\///g")
		echo "Preparing to delete targetserver '$config'"
		echo "{\"name\":\"$config\"}," >> $deletedConfigsFolder/targetServers.json ;;

		*) ;;
	esac
done

sed -i "$ s/.$//" $deletedConfigsFolder/caches.json
echo "]" >> $deletedConfigsFolder/caches.json

sed -i "$ s/.$//" $deletedConfigsFolder/kvms.json
echo "]" >> $deletedConfigsFolder/kvms.json

sed -i "$ s/.$//" $deletedConfigsFolder/targetServers.json
echo "]" >> $deletedConfigsFolder/targetServers.json

#mvn install -P$env -Dapigee.config.options=update -Dapigee.config.dir=./configs/modified-configs -Dusername=$username -Dpassword=$password clean
#mvn install -P$env -Dapigee.config.options=delete -Dapigee.config.dir=./configs/deleted-configs -Dusername=$username -Dpassword=$password clean