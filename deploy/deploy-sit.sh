#!/bin/bash

apiname=$1
username=$2
password=$3
overridedelay=$4
env_dev=\"dev\",
env_cit=\"cit\",
env_sit=\"sit\",
env_uat=\"uat\",
env_pre=\"pre\",
deployenv=""
cit_revision=""
sit_revision=""


while read line ;do
   if [[ $line =~ \"name\"  ]];then
      set $line
      echo  " ***********************************line is  $1 $2 $3 End"

                                if [ $deployenv = $env_cit ];then
                                        echo "Setting cit revision...."
                                        cit_revision=$3
                                        deployenv=""
                                fi
                                if [ $deployenv = $env_sit ];then
                                        echo "Setting sit revision...."
                                        sit_revision=$3
                                        deployenv=""
                                fi
                if [ $3 =  $env_cit ];then
                                        deployenv=$env_cit
                fi
                if [ $3 =  $env_sit ];then
                                        deployenv=$env_sit
                fi
                                echo "Results Deployment Env: $deployenv CIT Revision: $cit_revision SIT Revision: $sit_revision"
   fi
done < <(curl -u $username:$password -X GET "https://api.enterprise.apigee.com/v1/organizations/morrisons-pci-nonprod/apis/$apiname/deployments")
cit_revision_length=${#cit_revision}
cit_revision_length=$((cit_revision_length - 3))
cit_revision_num=${cit_revision:1:$cit_revision_length}
echo "cit_revision: $cit_revision_num"
sit_revision_length=${#sit_revision}
sit_revision_length=$((sit_revision_length - 3))
sit_revision_num=${sit_revision:1:$sit_revision_length}
echo "sit_revision: $sit_revision_num"

# Undeploy in SIT
curl -u $username:$password -X DELETE https://api.enterprise.apigee.com/v1/organizations/morrisons-pci-nonprod/environments/sit/apis/$apiname/revisions/$sit_revision_num/deployments
# Deploy in SIT
echo "curl -u $username:***** -X POST https://api.enterprise.apigee.com/v1/organizations/morrisons-pci-nonprod/environments/sit/apis/$apiname/revisions/$cit_revision_num/deployments?override=true&delay=$overridedelay"
curl -u $username:$password -X POST https://api.enterprise.apigee.com/v1/organizations/morrisons-pci-nonprod/environments/sit/apis/$apiname/revisions/$cit_revision_num/deployments?override=true&delay=$overridedelay

