# $1: env
# $2: apigee username
# $3: apigee password

echo wget "https://dev-api.morrisons.com/deployment/v1/environments/$1/revisions/@last/metadata" --header=Content-Type:application/xml --post-file=commit-info --http-user=$2 --http-password=$3 --auth-no-challenge
wget "https://dev-api.morrisons.com/deployment/v1/environments/$1/revisions/@last/metadata" --header=Content-Type:application/xml --post-file=commit-info --http-user=$2 --http-password=$3 --auth-no-challenge
# curl -X PUT "https://dev-api.morrisons.com/deployment/v1/environments/$1/revisions/@last/metadata" -H "Content-Type: application/xml" --data-binary "@commit-info" -u $2:$3