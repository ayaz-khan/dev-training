var accessControlRequestHeaders = context.getVariable('request.header.Access-Control-Request-Headers.values')+'';
var requestHeaders=context.getVariable('request.headers.names')+'';

if (accessControlRequestHeaders != '[]') {
	context.setVariable('flow.morrisons.corsHeaders',accessControlRequestHeaders.replace(/\[/g,"").replace(/\]/g,""));
} else {
	context.setVariable('flow.morrisons.corsHeaders',requestHeaders.replace(/\[/g,"").replace(/\]/g,""));
}