var cacheHit = context.getVariable("lookupcache.LookupCache.GetProxyPatternConfiguration.cachehit");

switch(cacheHit)
{
	//prepare variable flow.morrisons.configurationJson to populate in the Cache
	case false:
		var spikeGlobal = context.getVariable('flow.morrisons.spike.global');
		var configuration = {
				spikeGlobal: spikeGlobal
			};
		context.setVariable('flow.morrisons.configurationJson', JSON.stringify(configuration));
		break;
	//parse variable flow.morrisons.configurationJson from the Cache
	case true:
		var configurationJson = JSON.parse(context.getVariable('flow.morrisons.configurationJson')); //this is coming from lookupcache
		context.setVariable('flow.morrisons.spike.global', configurationJson.spikeGlobal);
		break;
}